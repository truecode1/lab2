import axios from "axios";
import fs from "fs";
import TelegramBotApi from "node-telegram-bot-api";

import { Api, TelegramClient } from "telegram";
import { StringSession } from "telegram/sessions/index.js";
import { readNumbers } from "./utils/utils.js";

import {
  CONFNAME,
  INFO,
  INSTRUCTIONS,
  MAIN_MENU,
  SETTINGS_MENU,
  LOAD_DATA_MENU,
  EXCEL_MIME_TYPE,
} from "./const.js";

// encodeURIComponent("https://disk.yandex.ru/i/TTkPbOZDQwyVdQ")

class Bot {
  constructor() {
    this.config = JSON.parse(fs.readFileSync(CONFNAME));

    this.bot = new TelegramBotApi(this.config.botToken, { polling: true });

    this.client = new TelegramClient(
      new StringSession(this.config.stringSession),
      this.config.apiId,
      this.config.apiHash,
      {
        connectionRetries: 5,
      }
    );

    this.isMailingInProgress = false;
    this.timer = null;
    this.waitForFile = false;

    this.listtenersInit();
  }

  listtenersInit() {
    // на старте проверяем авторизацию
    this.bot.onText(/\/start/, async (msg) => {
      if (this.checkAccess(msg.from.id)) {
        return this.adminStart(msg.chat.id);
      } else {
        return this.noAdminStart(msg.chat.id, msg.from.id, msg.from.username);
      }
    });

    // меню настроек
    this.bot.onText(/\/settings/, async (msg) => {
      if (this.checkAccess(msg.from.id)) {
        return this.showSettingsMenu(msg.chat.id);
      } else {
        return this.noAdminStart(msg.chat.id, msg.from.id, msg.from.username);
      }
    });

    // меню загрузки
    this.bot.onText(/\/load/, async (msg) => {
      if (this.checkAccess(msg.from.id)) {
        return this.showLoadDataMenu(msg.chat.id);
      } else {
        return this.noAdminStart(msg.chat.id, msg.from.id, msg.from.username);
      }
    });

    // вывод информации о проекте
    this.bot.onText(new RegExp(MAIN_MENU.INFO), (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      return this.bot.sendMessage(msg.from.id, INFO);
    });

    // вывод инструкции по использованию
    this.bot.onText(new RegExp(MAIN_MENU.INSTRUCTIONS), (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      return this.bot.sendMessage(msg.from.id, INSTRUCTIONS);
    });

    // начало рассылки
    this.bot.onText(new RegExp(MAIN_MENU.START_MAILING), async (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      if (this.config.stringSession == "") {
        return this.bot.sendMessage(
          chatId,
          `Пожалуйста запустите getStringSession.js и заполните в config.js stringSession`
        );
      }

      this.isMailingInProgress = true;

      const adminId = msg.from.id;

      if (this.timer) {
        clearInterval(this.timer);
      }
      this.timer = setInterval(async () => {
        const reciever = Object.values(this.config.data).find(
          (user) => !user.invited && !user.tried
        );

        if (!reciever) {
          return;
        }

        try {
          await this.sendInvite(reciever.id, adminId);
          this.config.data[reciever.phone].invited = true;
        } catch (err) {
          console.log(err);
        } finally {
          this.config.data[reciever.phone].tried = true;
          fs.writeFileSync(CONFNAME, JSON.stringify(this.config));
        }
      }, +this.config.period * 1000 * 60);

      await this.bot.sendMessage(adminId, `Рассылка начата`);

      return this.showMainMenu(adminId);
    });

    // конец рассылки
    this.bot.onText(new RegExp(MAIN_MENU.END_MAILING), async (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      clearInterval(this.timer);
      this.isMailingInProgress = false;

      await this.bot.sendMessage(msg.from.id, "Рассылка закончена");

      return this.showMainMenu(msg.from.id);
    });

    // вывод меню загрузки данных
    this.bot.onText(new RegExp(MAIN_MENU.LOAD_DATA), (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      return this.showLoadDataMenu(msg.chat.id);
    });

    // загрузка данных с гугл диска
    this.bot.onText(new RegExp(LOAD_DATA_MENU.GOOGLE), async (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      await this.bot.sendMessage(msg.from.id, "В РАЗРАБОТКЕ");
      return this.showLoadDataMenu(msg.chat.id);
    });

    // загрузка данных с яндекс диска
    this.bot.onText(new RegExp(LOAD_DATA_MENU.YANDEX), async (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      const url = await this.sendMessageWithReply(
        "Введите ссылку на публичный файл с таблицей:",
        msg.chat.id
      );

      try {
        const response = await axios.get(
          `https://cloud-api.yandex.net/v1/disk/public/resources/download?public_key=${url}`
        );

        const downloadUrl = response.data.href;

        const response2 = await axios.get(downloadUrl, {
          responseType: "arraybuffer",
        });

        if (response2.headers["content-type"] !== EXCEL_MIME_TYPE) {
          await this.bot.sendMessage(
            chatId,
            "Данный файл не является excel-файлом"
          );
          return this.showLoadDataMenu(msg.chat.id);
        }

        const filePath = `./yandex.xlsx`;
        const fileStream = fs.createWriteStream(filePath);
        await new Promise((resolve, reject) => {
          fileStream.write(response2.data, (err) => {
            fileStream.close();

            if (err) reject();
            resolve();
          });
        });

        let phoneNumbers = await readNumbers(filePath);

        // получаем id-шники и username'ы через клиентское апи
        const data = await this.getData(phoneNumbers);

        let newRecords = await this.saveData(data);

        return this.bot.sendMessage(
          msg.from.id,
          `Файл успешно загружен\nФактически новых записей: ${newRecords}`
        );
      } catch (err) {
        console.log(err);
        await this.bot.sendMessage(chatId, "Ошибка при загрузке файла");
      } finally {
        return this.showLoadDataMenu(msg.chat.id);
      }
    });

    // загрузка данных с локального файла
    this.bot.onText(new RegExp(LOAD_DATA_MENU.LOCAL), async (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      this.waitForFile = true;
      await this.bot.sendMessage(msg.from.id, "Загрузите файл с номерами");
    });

    // вывод меню настроек
    this.bot.onText(new RegExp(MAIN_MENU.SETTINGS), (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      return this.showSettingsMenu(msg.chat.id);
    });

    // периодичность рассылки
    this.bot.onText(new RegExp(SETTINGS_MENU.PERIOD), async (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      const period = +(await this.sendMessageWithReply(
        "Введите периодичность рассылки (в минутах, не меньше 20):",
        msg.chat.id
      ));

      if (period < 20 || isNaN(period)) {
        await this.bot.sendMessage(
          msg.chat.id,
          "Минимальное значение периода - 20 минут"
        );
      } else {
        this.config.period = period;
        fs.writeFileSync(CONFNAME, JSON.stringify(this.config));

        await this.bot.sendMessage(
          msg.chat.id,
          "Периодичность рассылки успешно изменена"
        );
      }

      return this.showSettingsMenu(msg.chat.id);
    });

    // канал для рассылки
    this.bot.onText(new RegExp(SETTINGS_MENU.CHANNEL), async (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      const channel = await this.sendMessageWithReply(
        "Введите ссылку на канал (обратите внимание, что пользователь, отправляющий рассылку, должен состоять в канале и иметь права на приглашение):",
        msg.chat.id
      );

      this.config.channel = channel;
      fs.writeFileSync(CONFNAME, JSON.stringify(this.config));

      await this.bot.sendMessage(
        msg.chat.id,
        "Канал-получатель рассылки успешно изменён"
      );

      return this.showSettingsMenu(msg.chat.id);
    });

    // кнопка назад
    this.bot.onText(new RegExp(SETTINGS_MENU.TO_MAIN_MENU), (msg) => {
      if (!this.checkAccess(msg.from.id)) return;

      return this.showMainMenu(msg.chat.id);
    });

    // парсим файл
    this.bot.on("document", async (msg) => {
      if (!this.checkAccess(msg.from.id, msg.from.username)) return;

      // если не ждём файла
      if (!this.waitForFile) return;

      if (
        msg.document.mime_type !=
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      ) {
        return bot.sendMessage(msg.from.id, "Данный файл не является excel");
      }

      const path = "./";
      let filename = await this.bot.downloadFile(msg.document.file_id, path);

      try {
        let phoneNumbers = await readNumbers(path + filename);

        // получаем id-шники и username'ы через клиентское апи
        const data = await this.getData(phoneNumbers);

        let newRecords = await this.saveData(data);

        return this.bot.sendMessage(
          msg.from.id,
          `Файл успешно загружен\nФактически новых записей: ${newRecords}`
        );
      } catch (err) {
        console.log(err);
        return this.bot.sendMessage(msg.from.id, "Не удалось загрузить файл");
      } finally {
        this.waitForFile = false;
      }
    });

    // логирование ошибок
    this.bot.on("polling_error", console.log);
  }

  // формируем данные с помощью клиентского апи
  async getData(phoneNumbers) {
    return phoneNumbers.map((pn) => ({
      id: 0,
      phone: pn,
      username: "",
    }));
  }

  // сохраняем данные в переменную и в файл
  async saveData(data) {
    let newRecords = 0;
    // заносим данные
    data.forEach((val) => {
      if (!this.config.data[val.phone]) {
        newRecords += 1;
        this.config.data[val.phone] = {
          ...val,
          invited: false,
          tried: false,
        };
      }
    });

    // сохраняем данные
    await new Promise((resolve, reject) =>
      fs.writeFile(CONFNAME, JSON.stringify(this.config), (err) => {
        if (err) {
          reject(console.log(err));
        }
        resolve();
      })
    );

    return newRecords;
  }

  // отправляем сообщение и дожидаемся ответа
  async sendMessageWithReply(message, chatId) {
    return new Promise(async (resolve) => {
      const prompt = await this.bot.sendMessage(chatId, message, {
        reply_markup: {
          force_reply: true,
        },
      });

      this.bot.onReplyToMessage(chatId, prompt.message_id, async (msg) => {
        resolve(msg.text);
      });
    });
  }

  showMainMenu(id) {
    this.bot.sendMessage(id, "Главное меню", {
      reply_markup: {
        keyboard: [
          [MAIN_MENU.INFO, MAIN_MENU.INSTRUCTIONS],
          [
            this.isMailingInProgress
              ? MAIN_MENU.END_MAILING
              : MAIN_MENU.START_MAILING,
          ],
          [MAIN_MENU.LOAD_DATA],
          [MAIN_MENU.SETTINGS],
        ],
      },
    });
  }

  showSettingsMenu(id) {
    this.bot.sendMessage(
      id,
      `
Текущая конфигурация:
Период рассылки - ${this.config.period}
Канал-получатель - ${this.config.channel}
`,
      {
        reply_markup: {
          keyboard: [
            [SETTINGS_MENU.PERIOD],
            [SETTINGS_MENU.CHANNEL],
            [SETTINGS_MENU.TO_MAIN_MENU],
          ],
        },
      }
    );
  }

  showLoadDataMenu(id) {
    this.bot.sendMessage(
      id,
      "Загрузка данных\n\nПравила по загрузке:\n1. Номера должны располагаться строго в первой колонке таблицы.\n2. Номера могут начинаться с +7, либо с 8.",
      {
        reply_markup: {
          keyboard: [
            [LOAD_DATA_MENU.GOOGLE],
            [LOAD_DATA_MENU.YANDEX],
            [LOAD_DATA_MENU.LOCAL],
            [LOAD_DATA_MENU.TO_MAIN_MENU],
          ],
        },
      }
    );
  }

  checkAccess(userId) {
    return this.config.admins.some((admin) => admin.id === userId);
  }

  async adminStart(chatId) {
    this.showMainMenu(chatId);
  }

  async noAdminStart(chatId, userId, username) {
    let login = await this.sendMessageWithReply("Введите логин:", chatId);
    let password = await this.sendMessageWithReply("Введите пароль:", chatId);

    if (this.config.login !== login || this.config.password !== password) {
      return this.bot.sendMessage(
        chatId,
        `Введены неверные данные авторизации. Для повторной попытки нажмите /start`
      );
    }

    this.config.admins.push({
      id: userId,
      username: username,
    });

    fs.writeFile(CONFNAME, JSON.stringify(this.config), (err) => {
      if (err) {
        console.log(err);
      }
    });

    return this.adminStart(chatId);
  }

  async sendInvite(recieverId, adminId) {
    // отправляем инвайт через клиентское апи
    await this.bot.sendMessage(
      adminId,
      `Отправка инвайта пользователю с ID = ${recieverId}`
    );
  }
}

const boter = new Bot();
