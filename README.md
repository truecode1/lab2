# lab2

## Кто и зачем?

Здравствуйте, эту работу подготовил студент АСУб-19-1 Лукаш Олег

Цель проекта - создать бота, с помощью которого можно осуществлять рассылки приглашений в канал

## Инструкция по использованию

Потыкать бота: @GigaChad11Bot

Инструкция по использованию:
1. Если в данный момент идёт рассылка, остановите её.
2. Нажмите на кнопку загрузки данных, и, следуя инструкции, заполните базу номеров.
3. Зайдите в Настройки. Укажите ссылку на канал-получатель, периодичность приглашений.
4. Вернитесь главное меню, нажмите "Начать рассылку приглашений".
5. Наслаждайтесь.

## Документация

Структура пользовательского меню:

![](docs/menu.png)

Видим, что меню позволяет загружать данные, настраивать рассылку, запускать рассылку. В планах также добавить просмотр статистики по рассылке.

Алгоритм сбора данных:

![](docs/save.png)

Поскольку пользователи приглашаются по одному, нужно предусмотреть условие для предотвращения зацикливания. Поэтому, независимо от удачной или неудачной попытки пригласить пользователя мы помечаем его, чтобы не приглашать его же при следующей рассылке.

Алгоритм рассылки:

![](docs/mailing.png)
